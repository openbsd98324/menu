
# Maths (online, web interface)

https://sagecell.sagemath.org/

SageMathCell project is an easy-to-use web interface to a free open-source mathematics software system SageMath. You can help SageMath by becoming a .
It allows embedding Sage computations into any webpage: check out our short instructions, a comprehensive description of capabilities, or Notebook Player to convert Jupyter notebooks into dynamic HTML pages!
Resources for your computation are provided by SageMath, Inc.. You can also set up your own server.

# Numworks, free open calculator 

NumWorks Graphing Calculator is described as 'NumWorks designed an intuitive and evolutive graphing calculator to make learning maths easier' and is a Calculator in the education & reference category. There are more than 100 alternatives to NumWorks Graphing Calculator for a variety of platforms, including Android, Mac, Android Tablet, Linux and F-Droid apps. The best NumWorks Graphing Calculator alternative is Wolfram Alpha, which is free. Other great apps like NumWorks Graphing Calculator are SpeedCrunch, Qalculate!, Fossify Calculator and OpenCalc.
filter to find the best alternatives
NumWorks Graphing Calculator alternatives are mainly Calculators but may also be Math Solvers. Filter by these or use the filter bar below if you want a narrower list of alternatives or looking for a specific functionality of NumWorks Graphing Calculator.

Web, Online Calc:

https://www.numworks.com/simulator/


# Teams, MS, Email Planet (Non-Free)


http://outlook.office365.com/mail


https://teams.microsoft.com/ 


https://outlook.office365.com/calendar/view/workweek


# Radio / Web Stream


http://direct.franceinfo.fr/live/franceinfo-midfi.mp3

https://25583.live.streamtheworld.com/HITRADIO_FAKTOR.aac


# ARMBIAN

=============== 
 Linux pinebook-pro 6.1.50-current-rockchip64 #3 SMP PREEMPT Wed Aug 30 14:11:13 UTC 2023 aarch64 GNU/Linux
 => ARMBIAN: Linux pinebook-pro 6.1.50-current-rockchip64 #3 SMP PREEMPT Wed Aug 30 14:11:13 UTC 2023 aarch64 GNU/Linux  
=============== 

fc5e4f3b208a6449dcf4c56ac5ee5c71  Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img
d5c2b92dd1ee1ae5c15f340d696a31ea  Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz
849cea7bb79030195be4b0d10eed093f57dba36806ac4927e9fe641a35a1c5d2  Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img
d4b8eed206a1c1eca7541340284ff986aa23633d3ad2283c8bbbe14cf08db6aa  Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz

 =============== 

# Pocketbook 

PB618 




# Libera Chat, IRC


https://kiwiirc.com/nextclient/irc.libera.chat/?channels=#ircchanteam 


https://kiwiirc.com/nextclient/irc.libera.chat/?channels=#linux





# EFI Live

https://gitlab.com/openbsd98324/EFI/-/raw/master/BOOT/huge.s

https://gitlab.com/openbsd98324/EFI/-/raw/master/BOOT/initrd.img





# Pinebook Pro Cinnamon

![](medias/1701804537-screenshot.png)

````
https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz
````





# PineBook Pro DEBIAN 

https://gitlab.com/openbsd98324/pinebook-pro-debian/

v3 extlinux to boot

gpt, with partition 1:

````
Device        Start      End  Sectors   Size Type
/dev/sda1     62500  1000000   937501 457.8M Microsoft basic data
/dev/sda2   1000001 29351935 28351935  13.5G Linux filesystem
/dev/sda3  29351936 60061695 30709760  14.6G Linux filesystem
````

Linux pinebook-pro 6.1.50-current-rockchip64 #3 SMP PREEMPT Wed Aug 30 14:11:13 UTC 2023 aarch64 GNU/Linux

debootstrap stable (debian) onto /dev/sda2 
arm64 

sda3 is ascii amd64 :) 


# PineBook Pro 

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-minimal-pbpro-23.02.img.xz

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-xfce4-pbpro-23.02.img.xz

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz

wget https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-gnome-pbpro-23.02.img.xz

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xzhttps://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-gnome-pbpro-23.02.img.xzhttps://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-xfce-pbpro-23.02.img.xz



- Custom

https://gitlab.com/openbsd98324/bootloader/-/raw/main/pub/pbpro/sdmmc/working/Manjaro-ARM-minimal-pbpro-23.02.img.gz

https://gitlab.com/openbsd98324/bootloader/-/raw/main/pub/pbpro/sdmmc/working/Manjaro-ARM-minimal-pbpro-23.02.img.gzhttps://gitlab.com/openbsd98324/bootloader/-/raw/main/pub/pbpro/sdmmc/working/Manjaro-ARM-minimal-pbpro-23.02.img.gz

 dd1ec6bbced2c00f2f9bd3227823f349  Manjaro-ARM-minimal-pbpro-23.02.img.gz 



# Ubuntu


Current Desktop  and dwld link 1 

https://releases.ubuntu.com/jammy/ubuntu-22.04.3-desktop-amd64.iso


# Download link 2

https://www.releases.ubuntu.com/22.04/ubuntu-22.04.3-desktop-amd64.iso


# pbpro gnome 23.02

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-gnome-pbpro-23.02.img.xz


#  Raspberry Pi Zero 

 wget -c --no-check-certificate   "https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi1_zero.img.gz"


# Raspbian raspberry pi  rpi3b, stretch (old)


https://gitlab.com/openbsd98324/pidesktop/-/raw/main/pidesktop1/2017-08-16-raspbian-stretch-lite.zip

# RPI4

https://downloads.raspberrypi.com/raspios_arm64/images/raspios_arm64-2023-10-10/2023-10-10-raspios-bookworm-arm64.img.xz

https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi4_400.img.gz



# raspios pi / desktop / tested ok

https://downloads.raspberrypi.com/raspios_arm64/images/raspios_arm64-2023-10-10/2023-10-10-raspios-bookworm-arm64.img.xz

https://downloads.raspberrypi.com/raspios_armhf/images/raspios_armhf-2023-10-10/2023-10-10-raspios-bookworm-armhf.img.xz


# pinebook pro manjaro boot sdmmc

23.02



# ubuntu 

kinetic 
5.19.0-46-generic

https://old-releases.ubuntu.com/releases/kinetic/ubuntu-22.10-desktop-amd64.iso



# Ubuntu 

https://releases.ubuntu.com/lunar/ubuntu-23.04-desktop-amd64.iso

# raspios raspberry pi 


1.) repository of stretch 

retropie 4.5.1
store raspberrypi-retropie-repository  



2.) ARMHF Work around to have a base 
with GCC and Make


store raspberrypi-retropie-rootfs


# RPI4 

https://gitlab.com/openbsd98324/retropie-rpi4/-/raw/main/archives/retropie-buster-4.8-rpi4_400.img.gz


# Manjaro ARM Lite (pinebook pro)

Bootloader SD/MMC : 
https://gitlab.com/openbsd98324/bootloader/-/raw/main/pub/pbpro/sdmmc/working/Manjaro-ARM-minimal-pbpro-23.02.img.gz



# Tested Bootloader from SD/MMC (pbpro, pinebook pro)

https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-minimal-pbpro-23.02.img.xz






# Qemu

Now you can emulate it on Qemu by using the following command:

´´´´
$ qemu-system-arm -kernel ~/qemu_vms/<your-kernel-qemu> -cpu arm1176 -m 256 -M versatilepb -serial stdio -append "root=/dev/sda2 rootfstype=ext4 rw" -hda ~/qemu_vms/<your-jessie-image.img> -redir tcp:5022::22 -no-reboot
´´´´

If you see GUI of the Raspbian OS, you need to get into the terminal. Use Win key to get the menu, then navigate with arrow keys until you find Terminal application as shown below.


# VM Machines

Ready to use Directory with vbox for virtualbox on PC, i.e. win, linux,...

https://gitlab.com/openbsd98324/virtualbox-testing/-/raw/main/pub/binary/linux/devuan/ascii/i386/devuan-ascii-i386-desktop-xfce.zip



![](https://gitlab.com/openbsd98324/virtualbox-testing/-/raw/main/medias/devuan-vm-vmplayer-ascii-desktop-xfce-default-v1.png)


# Manjaro ARM

https://gitlab.com/openbsd98324/manjaro-xbase5-plus


Base5 with kernel, and packages aarch64:

https://gitlab.com/openbsd98324/manjaro-pbpro-kde/-/raw/main/pinebookpro-firmware-base5-packages-v.1.0-1693229012.tar.gz?ref_type=heads


# SSHFS or FTPFS

https://wiki.archlinux.org/title/CurlFtpFS


# nanogrub2

https://gitlab.com/openbsd98324/nanogrub2/-/raw/main/nanogrub2-partition-mbr-backup-image-sda-mbr-sda1-grub-vmlinuz-preinstalled-rootfs-grubx86_64-1688805926.img.gz


https://gitlab.com/openbsd98324/devuan-live/-/raw/main/v1/ascii/amd64/DEVUAN.tar.gz



# devuan live


https://mirror.leaseweb.com/devuan/devuan_ascii/desktop-live/devuan_ascii_2.1_amd64_desktop-live.iso                      





# Terminal ide

https://gitlab.com/openbsd98324/apks/-/raw/main/pub/content/android/j5/terminalide-2.02.apk



# Pinebook pro

https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz

https://gitlab.com/openbsd98324/manjaro-22.06/-/raw/main/pub/aarch64/pbpro/Manjaro-ARM-kde-plasma-pbpro-22.06-1683855369-rootfs-rel1.0.tar.gz


# Raspberry PI 

 https://gitlab.com/openbsd98324/cmretropie/-/raw/main/cmretropie-4.5.1-rpi2_rpi3.img.gz 


# Android 

https://gitlab.com/openbsd98324/myapks


# Netbsd (zcat)

https://www.netbsd.org/~martin/rawrite32/help/rawrite32-en.htm

# Manjaro 22.06

img.xz: 

https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz


img.gz: 



rootfs: 

https://gitlab.com/openbsd98324/manjaro-22.06/-/raw/main/pub/aarch64/pbpro/Manjaro-ARM-kde-plasma-pbpro-22.06-1683855369-rootfs-rel1.0.tar.gz



# Linux Devuan (AMD64) 

https://mirror.leaseweb.com/devuan/devuan_ascii/installer-iso/devuan_ascii_2.1_amd64_dvd-1.iso



# OpenSUSE 

openSUSE-Tumbleweed-DVD-x86_64-Snapshot20230429-Media.iso 

https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz

https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/v2/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook-custom-4.9.tar.gz


# Ramdisk 

> Linux 

Create a ramdisk.

´´´´
   mkdir /ramdisk ; mount -t tmpfs -osize=1400M none   /ramdisk  ; mount ; mkdir 777 /ramdisk 
´´´´


# Subversion (SVN)

´´´´
   mkdir ~/src ; cd ; cd src ; svn checkout --username=theusername   https://svn.riouxsvn.com/theprojectidname  
´´´´
 

# Further links 

 http://outlook.office365.com/mail/


Best place to start with ARM: https://retropie.org.uk/download/, https://www.raspberrypi.com/software/operating-systems/ , https://manjaro.org/download/, 

"https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz" 

   





